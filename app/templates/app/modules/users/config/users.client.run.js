'use strict';

angular.module('users').run(['$rootScope', '$location','growlMessages','$state','$window','$urlRouter','Config',
	function($rootScope, $location,growlMessages, $state, $window, $urlRouter, Config) {
	
		
		$rootScope.$on('$stateChangeSuccess', function(event,toState){
	      growlMessages.destroyAllMessages();
	      event.preventDefault();
	    });
	}
]);