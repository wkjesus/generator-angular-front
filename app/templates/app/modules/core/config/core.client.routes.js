'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider',
	function($stateProvider, $urlRouterProvider, markedProvider, $provide) {
		
    // Redirect to home view when route not found
    $urlRouterProvider.otherwise(function ($injector) {
      var $state = $injector.get('$state');
      $state.go('app.home');
    });

		// Home state routing
		$stateProvider
		.state('app', {
          abstract: true,
          views: {
            root: {
              controller:'CoreController',
              controllerAs:'vmCore',
              templateUrl: '/app/modules/core/views/core.client.view.html'
            }
          }
      })
      .state('app.home', {
        url:'/',
		views: {
			'content@app': {
		  	controller:'HomeController',
		 	controllerAs:'vmHome',
		  	templateUrl: '/app/modules/core/views/home.client.view.html'
			}
		},
        ncyBreadcrumb: {
		    label: 'Home' // angular-breadcrumb's configuration
		}
      })
    .state('app.about', {
        url:'/about',
        views: {
          'content@app': {
            // controller:'AboutController',
            // controllerAs:'vmHome',
            templateUrl: '/app/modules/core/views/about.client.view.html'
          }
        },
            ncyBreadcrumb: {
            label: 'About' // angular-breadcrumb's configuration
        }
      });
	}
]);