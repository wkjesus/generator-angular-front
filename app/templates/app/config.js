'use strict';

// Modulos de angular registrados
var appModulosApplication = [];

var ApplicationConfiguration = (function() {

	var applicationModuleName = 'demo',
		applicationModuleVendorDependencies = [
			'app.Config',
			'ngRoute',
			'ngResource',
			'ngAnimate',
			'ngCookies',
			'ngStorage',
			'ngSanitize',
			'ngTouch',
			'ui.router',
			'permission',
			'permission.ui',
			'permission.ng',
			'ui.bootstrap',
			'angularMoment',
			'swipe',
			'ngBootstrap',
			'truncate',
			'uiSwitch',
			'toaster',
			'ngAside',
			'vAccordion',
			'vButton',
			'oitozero.ngSweetAlert',
			'angular-notification-icons',
			'angular-ladda',
			'angularAwesomeSlider',
			'slickCarousel',
			'cfp.loadingBar',
			'angular-loading-bar',
			'ncy-angular-breadcrumb',
			'duScroll',
			'pascalprecht.translate',
			'FBAngular',
			'xeditable',
			'mwl.calendar',
			'angularSoap',
			'ng-nestable',
			'angular-uuid',
			'LocalStorageModule',
			'blockUI',
			'ngTable',
			'angularFileUpload'
		],
		registerModule = function(moduleName) {
			appModulosApplication.push(moduleName); // Registramos el modulo para su consumo
			angular.module(moduleName, []);
			angular.module(applicationModuleName).requires.push(moduleName);
		};

	return {
		applicationModuleName: applicationModuleName,
		applicationModuleVendorDependencies: applicationModuleVendorDependencies,
		registerModule: registerModule
	};
})();
