/* jshint ignore:start */
 angular.module("app.Config", [])
.constant("api", "http://localhost/apisigmod/")
.constant("security", "http://localhost/apisigmod/")
.constant("APP_MEDIAQUERY", {
  "desktopXL": 1200,
  "desktop": 992,
  "tablet": 768,
  "mobile": 480
})
.constant("app", {
  "name": "Sigmod",
  "author": "Wsoft",
  "description": "Sistema para la gestión de documentos y operaciones de campo.",
  "version": "1.0.0",
  "isMobile": false,
  "defaultLayout": {
    "isNavbarFixed": true,
    "isSidebarFixed": true,
    "isSidebarClosed": false,
    "isFooterFixed": false,
    "isBoxedPage": false,
    "theme": "lyt4-theme-1",
    "logo": "/app/assets/img/logo.png",
    "logoCollapsed": "/app/assets/img/logo-collapsed.png"
  },
  "layout": {
    "isNavbarFixed": true,
    "isSidebarFixed": true,
    "isSidebarClosed": false,
    "isFooterFixed": false,
    "isBoxedPage": false,
    "theme": "lyt4-theme-1",
    "logo": "/app/assets/img/logo.png",
    "logoCollapsed": "/app/assets/img/logo-collapsed.png"
  }
});
 
 /* jshint ignore:end */