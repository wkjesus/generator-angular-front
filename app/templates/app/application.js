'use strict';

angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

angular.module(ApplicationConfiguration.applicationModuleName).config([
	'$locationProvider',
	'$httpProvider','$breadcrumbProvider',
	function($locationProvider, $httpProvider,$breadcrumbProvider) {
		$locationProvider.hashPrefix('!');
        $breadcrumbProvider.setOptions({
          prefixStateName: 'app.home',
          templateUrl: '/app/modules/core/views/breadcrumbs.client.view.html'
        });
		//$httpProvider.interceptors.push('RequestsInterceptor');
	}
]);

angular.element(document).ready(function() {
	if (window.location.hash === '#_=_') window.location.hash = '#!';
	angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});