'use strict';
var util = require('util'),
	path = require('path'),
	yeoman = require('yeoman-generator'),
	chalk = require('chalk'),
	fs = require('fs');

var MeanGenerator = yeoman.generators.Base.extend({
	init: function() {
		// read the local package file
		this.pkg = yeoman.file.readJSON(path.join(__dirname, '../package.json'));

		// invoke npm install on finish
		this.on('end', function() {
			if (!this.options['skip-install']) {
				this.npmInstall();
			}
			this.installDependencies({
		    skipInstall: this.options['skip-install']
		  });

			// this.installDependencies({
			//   bower: true,
			//   npm: true,
			//   callback: function () {
			//     console.log('Descargando Librerias!');
			//   }
			// });

		});

		// have Yeoman greet the user
		console.log(this.yeoman);
		
	},

	askForApplicationDetails: function() {
		var done = this.async();
		
		//console.log(branch);
		//console.log('Terminal size: ' + process.stdout.columns + 'x' + process.stdout.rows);

		
		var _this = this;


		var prompts = [{
			name: 'appName',
			//message: 'What would you like to call your application?',
			message: '¿Cual sera el nombre de tu aplicación?',
			default: 'miApp'
		}, {
			name: 'appDescription',
			//message: 'How would you describe your application?',
			message: '¿Como describirias tu aplicación?',
			default: ''
		}, {
			name: 'appKeywords',
			//message: 'How would you describe your application in comma seperated key words?',
			message: '¿Como describirias tu aplicación con palabras clave?',
			default: ''
		}, {
			name: 'appAuthor',
			//message: 'What is your company/author name?'
			message: '¿Cual es el nombre del autor?'
		}];

		this.prompt(prompts, function(props) {
			this.appName = props.appName;
			this.appDescription = props.appDescription;
			this.appKeywords = props.appKeywords;
			this.appAuthor = props.appAuthor;
			this.addArticleExample = props.addArticleExample;

			this.slugifiedAppName = this._.slugify(this.appName);
			this.humanizedAppName = this._.humanize(this.appName);
			this.capitalizedAppAuthor = this._.capitalize(this.appAuthor);

			done();
		}.bind(this));
		
	},

	// copyApplicationStyles: function() {
	// 	var done = this.async();
	// 	var prompts = [{
	// 		type: 'list',
	// 		name: 'styles',
	// 		message: 'Seleccione el Template para la Aplicación',
	// 		choices: ['Bootstrap', 'Ninguno'],
	// 		filter:function(val){
	// 			//console.log(val);
	// 			var filterMap={'Bootstrap':'Bootstrap', 'Ninguno':''};
	// 			return filterMap[val];
	// 		}
	// 	}];

	// 	this.prompt(prompts, function(props) {
	// 		//console.log(props);
	// 		this.selectedStyle = props.styles;
	// 		done();
	// 	}.bind(this));
	// 	// 
		
	// },

	copyApplicationFolder: function() {
		// Copy application folder
		this.mkdir('app');
		
		var baseLayoutPath = '';
		var baseIndexPath = '';
		var corePath = '';

		// switch(this.selectedStyle){
		// 	case 'Bootstrap':
				corePath = 'app/src/core_ace.view.raw.html'
		// 		break;
		// 	default:
		// 		corePath='app/src/core.view.raw.html'
		// 		break;
		// }

		// Create app folders
		//this.mkdir('app');
		this.mkdir('app/modules');
		this.mkdir('app/src');
		this.mkdir('app/assets');
		this.mkdir('app/assets/avatars');

		// Copy app folder content
		this.copy('app/app.config.js');
		this.copy('app/application.js');
		this.copy('app/config.app.json');
		this.copy('app/config.js');
		this.copy('app/global.app.json');
		this.copy('app/humans.txt');
		this.copy('app/robots.txt');

		// Copy app folder modules
		this.directory('app/modules/users');

		// Copy core module files
		this.mkdir('app/modules/core/css');
		// this.copy('app/modules/core/css/core.css');
		// //this.copy('app/modules/core/css/loading-indicator.css');
		
		// this.copy('app/modules/core/css/animate.css');
		this.directory('app/modules/core/css');
				
		this.directory('app/modules/core/config');
		this.directory('app/modules/core/controllers');
		this.directory('app/modules/core/img');
		this.directory('app/modules/core/services');
		this.directory('app/modules/core/directives');
		this.directory('app/modules/core/tests');
		this.directory('app/modules/core/views/templates');
		this.copy('app/modules/core/views/home.client.view.html');
		this.copy('app/modules/core/views/about.client.view.html');
		this.copy('app/modules/core/views/breadcrumb.client.view.html');
		this.copy('app/modules/core/core.client.module.js');

		this.copy(corePath, 'app/modules/core/views/core.client.view.html');
		this.copy('app/src/index.view.raw.html');
		this.copy('app/assets/avatars/user.jpg');

		// Copy project files
		// this.copy('karma.conf.js');
		 this.copy('gulpfile.js');
		// this.copy('server.js');
		this.copy('Procfile');
		this.copy('LICENSE.md');

		// Copy project hidden files
		this.copy('bowerrc', '.bowerrc');
		this.copy('.csslintrc', '.csslintrc');
		this.copy('.jshintrc', '.jshintrc');
		this.copy('gitignore', '.gitignore');
		this.copy('slugignore', '.slugignore');
		this.copy('.htmlhintrc', '.htmlhintrc');

		// Copy app folder modules
		this.directory('task');
		//this.copy('travis.yml', '.travis.yml');
	},

	// renderApplicationEnvironmentConfigFiles: function() {
	// 	this.template('config/env/_all.js', 'config/env/all.js');
	// 	this.template('config/env/_development.js', 'config/env/development.js');
	// 	this.template('config/env/_production.js', 'config/env/production.js');
	// 	this.template('config/env/_test.js', 'config/env/test.js');
	// },

	// renderAngularApplicationConfigFile: function() {
	// 	this.template('app/_config.js', 'app/config.js');
	// },

	renderCoreModuleFiles: function() {
		//this.template('app/modules/core/views/_header.client.view.html', 'app/modules/core/views/header.client.view.html');
	},

	renderApplicationDependenciesFiles: function() {
		this.template('_package.json', 'package.json');
		this.template('_bower.json', 'bower.json');
		this.template('_README.md', 'README.md');

		//var done = this.async();
    	
	}
});

module.exports = MeanGenerator;
