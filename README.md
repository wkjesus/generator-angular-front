
## Comandos

Comandos disponibles:

* [angular-front](#generador-de-aplicaciones)
* [angular-front:angular-module](#generador-de-modulos-angularjs)
* [angular-front:angular-route](#generador-de-rutas-angularjs)
* [angular-front:angular-controller](#generador-de-controladores-angularjs)
* [angular-front:angular-view](#generador-de-vistas-angularjs)
* [angular-front:angular-service](#generador-de-servicios-angularjs)
* [angular-front:angular-directive](#generador-de-directivas-angularjs)
* [angular-front:angular-filter](#generador-de-filtros-angularjs)
* [angular-front:angular-config](#generador-de-configuraciones-angularjs)
* [angular-front:angular-test](#generador-de-pruebas-unitarias-angularjs)

**Nota: Los generadores deben ejecutarse en la carpeta raíz de la aplicación.**


## Comando de Aplicaciones

El generador de aplicaciones creará una copia nueva de una aplicación angular-front en la carpeta donde se ejecute. Para crear una aplicación WEB, crear una nueva carpeta y en la carpeta nueva utilizar:

```
$ yo angular-front
```

El generador hará algunas preguntas acerca de la nueva aplicación y la generará por ti. Cuando la instalación termine, serás capaz de usar grunt para inicializar la aplicación:

```
$ gulp
```

## Comando de módulos AngularJS 

Este generador se utiliza para generar nuevos módulos de AngularJS, el cual creará la estructura de carpeta apropiada y la configuración necesaria para la inicialización del módulo. Para crear un nuevo módulo solo hay que ejecutar:

```
$ yo angular-front:angular-module <nombre del modulo>
```

El generador preguntará más información de la estructura de carpeta y creará un nuevo módulo vacio.

## Comando de Rutas AngularJS

Muchas veces un módulo necesita crear una ruta nueva, es por ello que este generador ayudar a crear una vista, controlador y la configuración necesaria de la ruta en el archivo **routes.js** del módulo. Si no encuentra el archivo de rutas el generador automáticamente generara uno nuevo.


```
$ yo angular-front:angular-route <nombre del estado>
```
El generador pedirá información acerca del controlador, vista y URL de la ruta, y generará los archivos necesarios.


## Comando de Controladores AngularJS

El generador creará un controlador AngularJS en la carpeta **controllers** del módulo especificado y debe ser utilizado el siguiente comando:


```
$ yo angular-front:angular-controller <nombre de controlador>
```

El generador preguntará por el nombre del módulo donde deseas crear el nuevo controlador, y creará el nuevo archivo del controlador en la carpeta **controllers** y el archivo de prueba unitaria en la carpeta **tests** del módulo seleccionado.


**Importante** El nombre del controlador será pasado del argumento del generador.



## Comando de Vistas AngularJS

Este generador creará una nueva Vista AngularJS en la carpeta **views** del módulo especificado, y permitirá definir una ruta, utilizando el comando:


```
$ yo angular-front:angular-view <nombre de vista>
```

El generador preguntará por el nombre del módulo donde se creará la nueva vista, e información adicional de la ruta. Creará un nuevo archivo bajo la carpeta **views** y agregará el código necesario para la ruta en el archivo **routes.js** del módulo seleccionado.


## Comando de Servicios AngularJS

El generador creará un Servicio AngularJS bajo la carpeta **services** del módulo seleccionado, y debe ejecutarse el siguiente comando:

```
$ yo angular-front:angular-service <nombre de servicio>
```

El generador preguntará en qué módulo deseas crear el servicios y esto creará un nuevo archivo bajo la carpeta **services** del módulo seleccionado.


## Comando de Directivas AngularJS

El generador creará un Servicio AngularJS bajo la carpeta **directives** del módulo seleccionado, y debe ejecutarse el siguiente comando:

```
$ yo angular-front:angular-directive <nombre directiva>
```

El generador preguntará en qué módulo deseas crear el servicios y esto creará un nuevo archivo bajo la carpeta **directives** del módulo seleccionado.



## Comando de Filtros AngularJS

El generador creará un Servicio AngularJS bajo la carpeta **filters** del módulo seleccionado, y debe ejecutarse el siguiente comando:


```
$ yo angular-front:angular-filter <nombre de filtro>
```

El generador preguntará en qué módulo deseas crear el servicios y esto creará un nuevo archivo bajo la carpeta **filters** del módulo seleccionado.


## Comando de Configuraciones AngularJS

El generador creará un Servicio AngularJS bajo la carpeta **config** del módulo seleccionado, y debe ejecutarse el siguiente comando:


```
$ yo angular-front:angular-config <nombre de configuracion>
```

El generador preguntará en qué módulo deseas crear el servicios y esto creará un nuevo archivo bajo la carpeta **config** del módulo seleccionado.


**IMPORTANTE** el nombre del controlador debe pasarse como argumento del generador. 
