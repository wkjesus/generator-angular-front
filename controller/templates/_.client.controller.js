'use strict';

angular.module('<%= slugifiedModuleName %>').controller('<%= classifiedControllerName %>Controller', ['$scope',
function($scope) {
    var vm<%= classifiedControllerName %> = this;
		// <%= humanizedControllerName %> controller logic
		// ...
}]);