'use strict';

//Setting up route
angular.module('<%= slugifiedModuleName %>').config(['$stateProvider',
	function($stateProvider) {
		// <%= humanizedModuleName %> state routing
		$stateProvider
		//.state('app.<%= slugifiedName %>', {
		// 	url: '/<%= slugifiedRoutePath %>',
		// 	templateUrl: 'modules/<%= slugifiedModuleName %>/views/<%= slugifiedViewName %>.client.view.html'
		// });
		.state('app.<%= slugifiedName %>', {
        url:'/<%= slugifiedRoutePath %>',
		views: {
			'content@app': {
		  	controller:'<%= classifiedControllerName %>Controller',
		 	controllerAs:'vm<%= classifiedControllerName %>',
		  	templateUrl: '/app/modules/<%= slugifiedModuleName %>/views/<%= slugifiedViewName %>.client.view.html'
			}
		},
        ncyBreadcrumb: {
		    label: '<%= classifiedControllerName %>' // angular-breadcrumb's configuration
		}
      });
	}
]);